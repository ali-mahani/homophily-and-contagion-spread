from classes.network import Network
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx


class Analysis:
    def __init__(self, graph_file_name, name, data_path, results_path):
        self.data_path = data_path
        self.results_path = results_path
        self.graph_file_name = f'{data_path}/{graph_file_name}'
        self.name = name
        self.graph = nx.read_gpickle(self.graph_file_name)

    def network_properties(self):
        size = nx.number_of_nodes(self.graph)
        edges = nx.number_of_edges(self.graph)
        
        degrees = np.zeros(size)
        for index, degree in nx.degree(self.graph):
            degrees[index] = degree
        degree_max = np.max(degrees)
        degree_min = np.min(degrees)
        degree_avg = np.average(degrees)

        clustering_avg = nx.average_clustering(self.graph)

        connected_components = nx.connected_components(self.graph)
        diameter = None
        max_connected_size = 0
        for component in connected_components:
            if len(component) > max_connected_size:
                connected_graph = nx.subgraph(self.graph, component)
                diameter = nx.diameter(connected_graph)
                max_connected_size = len(component)

        print(f'Nodes: {size}')
        print(f'Edges: {edges}')
        print(f'd_avg: {degree_avg}')
        print(f'd_max: {degree_max}')
        print(f'd_min: {degree_min}')
        print(f'c_avg: {clustering_avg}')
        print(f'diam: {diameter}')

    def threshold_distributions(self, samples_number=50, p_h_start=0.1, p_h_stop=0.9, p_h_samples=9):
        size = nx.number_of_nodes(self.graph)
        edges = nx.number_of_edges(self.graph)

        p_h_s = np.linspace(p_h_start, p_h_stop, p_h_samples)
        low_agents_numbers_ensembles = np.zeros((samples_number, len(p_h_s)))
        high_agents_numbers_ensembles = np.zeros((samples_number, len(p_h_s)))
        same_low_edges_numbers_ensembles = np.zeros((samples_number, len(p_h_s)))
        same_high_edges_numbers_ensembles = np.zeros((samples_number, len(p_h_s)))
        same_edges_numbers_ensembles = np.zeros((samples_number, len(p_h_s)))
        different_edges_numbers_ensembles = np.zeros((samples_number, len(p_h_s)))

        for sample_number in range(samples_number):
            for p_h_index, p_h in enumerate(p_h_s):
                print(f'\rp_h: {p_h} \t sample: {sample_number + 1}', end='')
                network = Network(self.graph, p_h, 0, 0)
                low_agents_numbers_ensembles[sample_number][p_h_index] = network.number_of_low_agents()
                high_agents_numbers_ensembles[sample_number][p_h_index] = network.number_of_high_agents()
                same_low_edges_numbers_ensembles[sample_number][p_h_index] = network.number_of_same_low_edges()
                same_high_edges_numbers_ensembles[sample_number][p_h_index] = network.number_of_same_high_edges()
                same_edges_numbers_ensembles[sample_number][p_h_index] = network.number_of_same_edges()
                different_edges_numbers_ensembles[sample_number][p_h_index] = network.number_of_different_edge()

        data = {
            'p_h_s': p_h_s,
            'samples_number': samples_number,
            'size': size,
            'edges': edges,
            'low_agents': low_agents_numbers_ensembles,
            'high_agents': high_agents_numbers_ensembles,
            'same_low_edges': same_low_edges_numbers_ensembles,
            'same_high_edges': same_high_edges_numbers_ensembles,
            'same_edges': same_edges_numbers_ensembles,
            'different_edges': different_edges_numbers_ensembles,
        }
        file_name = f'{self.data_path}/threshold_distributions_{self.name}_{p_h_start}_{p_h_stop}_{p_h_samples}_{samples_number}.npy'
        np.save(file_name, data)
        self.show_threshold_distributions(file_name)

    def show_threshold_distributions(self, file_name):
        data = np.load(file_name, allow_pickle=True).tolist()
        p_h_s = data['p_h_s']
        samples_number = data['samples_number']
        size = data['size']
        edges = data['edges']
        low_agents_numbers_ensembles = data['low_agents']
        high_agents_numbers_ensembles = data['high_agents']
        same_low_edges_numbers_ensembles = data['same_low_edges']
        same_high_edges_numbers_ensembles = data['same_high_edges']
        same_edges_numbers_ensembles = data['same_edges']
        different_edges_numbers_ensembles = data['different_edges']

        low_agents_fraction_avg = np.average(low_agents_numbers_ensembles, axis=0) / size
        low_agents_fraction_error = np.std(low_agents_numbers_ensembles, axis=0) / size / np.sqrt(samples_number - 1)
        high_agents_fraction_avg = np.average(high_agents_numbers_ensembles, axis=0) / size
        high_agents_fraction_error = np.std(high_agents_numbers_ensembles, axis=0) / size / np.sqrt(samples_number - 1)
        same_low_edges_fraction_avg = np.average(same_low_edges_numbers_ensembles, axis=0) / edges
        same_low_edges_fraction_error = np.std(same_low_edges_numbers_ensembles, axis=0) / edges / np.sqrt(
            samples_number - 1)
        same_high_edges_fraction_avg = np.average(same_high_edges_numbers_ensembles, axis=0) / edges
        same_high_edges_fraction_error = np.std(same_high_edges_numbers_ensembles, axis=0) / edges / np.sqrt(
            samples_number - 1)
        same_edges_fraction_avg = np.average(same_edges_numbers_ensembles, axis=0) / edges
        same_edges_fraction_error = np.std(same_edges_numbers_ensembles, axis=0) / edges / np.sqrt(samples_number - 1)
        different_edges_fraction_avg = np.average(different_edges_numbers_ensembles, axis=0) / edges
        different_edges_fraction_error = np.std(different_edges_numbers_ensembles, axis=0) / edges / np.sqrt(
            samples_number - 1)

        plt.errorbar(p_h_s, low_agents_fraction_avg, yerr=low_agents_fraction_error, marker='o')
        plt.errorbar(p_h_s, high_agents_fraction_avg, yerr=high_agents_fraction_error, marker='o')
        plt.xlabel(r'$P_h$')
        plt.ylabel('Fraction of Agents')
        plt.legend([r'$\theta_l$', r'$\theta_h$'])
        plt.savefig(
            f'{self.results_path}/threshold_agents_fraction_distributions_{self.name}_{p_h_s[0]}_{p_h_s[-1]}_{len(p_h_s)}_{samples_number}.jpg')
        plt.show()

        plt.errorbar(p_h_s, same_low_edges_fraction_avg, yerr=same_low_edges_fraction_error, marker='o')
        plt.errorbar(p_h_s, same_high_edges_fraction_avg, yerr=same_high_edges_fraction_error, marker='o')
        plt.errorbar(p_h_s, same_edges_fraction_avg, yerr=same_edges_fraction_error, marker='o')
        plt.errorbar(p_h_s, different_edges_fraction_avg, yerr=different_edges_fraction_error, marker='o')
        plt.xlabel(r'$P_h$')
        plt.ylabel('Fraction of Edges')
        plt.legend(['same low', 'same high', 'same', 'different'])
        plt.savefig(
            f'{self.results_path}/threshold_edges_fraction_distributions_{self.name}_{p_h_s[0]}_{p_h_s[-1]}_{len(p_h_s)}_{samples_number}.jpg')
        plt.show()

    def state_distributions(self, low, p_p, t_s, samples_number=50, p_h_start=0.1, p_h_stop=1.0, p_h_samples=10):
        size = nx.number_of_nodes(self.graph)
        edges = nx.number_of_edges(self.graph)
        p_h_s = np.linspace(p_h_start, p_h_stop, p_h_samples)
        revolted_agents_numbers_ensembles = np.zeros((samples_number, len(p_h_s), len(t_s)))

        for sample_number in range(samples_number):
            for p_h_index, p_h in enumerate(p_h_s):
                network = Network(self.graph, p_h, low, p_p)
                prev_time = 0
                for time_index, time in enumerate(t_s):
                    print(f'\rp_h: {p_h} \t time: {time} \t sample: {sample_number + 1}', end='')
                    network.render(time - prev_time)
                    prev_time = time
                    revolted_agents_numbers_ensembles[sample_number][p_h_index][
                        time_index] = network.number_of_revolted_agents()

        data = {
            'p_h_s': p_h_s,
            't_s': t_s,
            'low': low,
            'p_p': p_p,
            'samples_number': samples_number,
            'size': size,
            'edges': edges,
            'revolted_agents': revolted_agents_numbers_ensembles,
        }
        file_name = f'{self.data_path}/state_distributions_{self.name}_{low}_{p_p}_{t_s}_{p_h_start}_{p_h_stop}_{p_h_samples}_{samples_number}.npy'
        np.save(file_name, data)
        self.show_state_distributions(file_name)

    def show_state_distributions(self, file_name):
        data = np.load(file_name, allow_pickle=True).tolist()
        p_h_s = data['p_h_s']
        t_s = data['t_s']
        low = data['low']
        p_p = data['p_p']
        samples_number = data['samples_number']
        size = data['size']
        edges = data['edges']
        revolted_agents_numbers_ensembles = data['revolted_agents']

        legends = []
        for index, time in enumerate(t_s):
            revolted_agents_numbers = revolted_agents_numbers_ensembles[:, :, index]
            revolted_agents_fraction_avg = np.average(revolted_agents_numbers, axis=0) / size
            revolted_agents_fraction_error = np.std(revolted_agents_numbers, axis=0) / size / np.sqrt(
                samples_number - 1)

            plt.errorbar(p_h_s, revolted_agents_fraction_avg, yerr=revolted_agents_fraction_error, marker='o')
            legends.append(rf'$t = {time}$')

        plt.xlabel(r'$P_h$')
        plt.ylabel('Fraction of Agents')
        plt.legend(legends)
        plt.savefig(
            f'{self.results_path}/revolted_agents_fraction_distributions_{self.name}_{low}_{p_p}_{t_s}_{p_h_s[0]}_{p_h_s[-1]}_{len(p_h_s)}_{samples_number}.jpg')
        plt.show()

    def contagion_speed_distributions(self, low, p_p, size_fraction_s, samples_number=50, p_h_start=0.1, p_h_stop=1.0,
                                      p_h_samples=10):
        max_step = 200
        size = nx.number_of_nodes(self.graph)
        edges = nx.number_of_edges(self.graph)
        p_h_s = np.linspace(p_h_start, p_h_stop, p_h_samples)
        contagion_speed_ensembles = np.zeros((samples_number, len(p_h_s), len(size_fraction_s)))

        for sample_number in range(samples_number):
            for p_h_index, p_h in enumerate(p_h_s):
                network = Network(self.graph, p_h, low, p_p)
                revolted_agents_fraction = network.number_of_revolted_agents() / size
                for size_index, size_fraction in enumerate(size_fraction_s):
                    while revolted_agents_fraction < size_fraction:
                        print(f'\rp_h: {p_h}\tsize: {size_fraction}\tsample: {sample_number + 1}\ttime: {network.time}',
                              end='')
                        network.render(1)
                        revolted_agents_fraction = network.number_of_revolted_agents() / size

                        if network.time > max_step:
                            network = Network(self.graph, p_h, low, p_p)

                    contagion_speed_ensembles[sample_number][p_h_index][size_index] = network.time

        data = {
            'p_h_s': p_h_s,
            'size_s': size_fraction_s,
            'low': low,
            'p_p': p_p,
            'samples_number': samples_number,
            'size': size,
            'edges': edges,
            'contagion_speed': contagion_speed_ensembles,
        }
        file_name = f'{self.data_path}/spread_time_distributions_{self.name}_{low}_{p_p}_{size_fraction_s}_{p_h_start}_{p_h_stop}_{p_h_samples}_{samples_number}.npy'
        np.save(file_name, data)
        self.show_contagion_speed_distributions(file_name)

    def show_contagion_speed_distributions(self, file_name):
        data = np.load(file_name, allow_pickle=True).tolist()
        p_h_s = data['p_h_s']
        size_fraction_s = data['size_s']
        low = data['low']
        p_p = data['p_p']
        samples_number = data['samples_number']
        size = data['size']
        edges = data['edges']
        contagion_speed_ensembles = data['contagion_speed']

        legends = []
        for index, size_fraction in enumerate(size_fraction_s):
            contagion_speeds = contagion_speed_ensembles[:, :, index]
            contagion_speed_avg = np.average(contagion_speeds, axis=0)
            contagion_speed_error = np.std(contagion_speeds, axis=0) / np.sqrt(samples_number - 1)

            plt.errorbar(p_h_s, contagion_speed_avg, yerr=contagion_speed_error, marker='o')
            legends.append(rf'$s = {size_fraction}$')

        plt.xlabel(r'$P_h$')
        plt.ylabel('Time to Spread Size')
        plt.legend(legends)
        plt.savefig(
            f'{self.results_path}/spread_time_distributions_{self.name}_{low}_{p_p}_{size_fraction_s}_{p_h_s[0]}_{p_h_s[-1]}_{len(p_h_s)}_{samples_number}.jpg')
        plt.show()

    def cascade_likelihood_distributions(self, p_p, low_s, samples_number=50, p_h_start=0.1,
                                         p_h_stop=0.9, p_h_samples=9):
        max_time = 100
        fraction_threshold = 0.9
        size = nx.number_of_nodes(self.graph)
        edges = nx.number_of_edges(self.graph)
        p_h_s = np.linspace(p_h_start, p_h_stop, p_h_samples)
        cascade_likelihood_ensembles = np.zeros((len(p_h_s), len(low_s)))

        for sample_number in range(samples_number):
            for p_h_index, p_h in enumerate(p_h_s):
                for low_index, low in enumerate(low_s):
                    network = Network(self.graph, p_h, low, p_p)
                    for step in range(max_time):
                        print(f'\rp_h: {p_h}\tlow: {low}\tsample: {sample_number + 1}\ttime: {network.time}',
                              end='')
                        network.render(1)
                        revolted_agents_fraction = network.number_of_revolted_agents() / size
                        if revolted_agents_fraction >= fraction_threshold:
                            cascade_likelihood_ensembles[p_h_index][low_index] += 1
                            break

        cascade_likelihood_ensembles /= samples_number

        data = {
            'p_h_s': p_h_s,
            'low_s': low_s,
            'p_p': p_p,
            'samples_number': samples_number,
            'size': size,
            'edges': edges,
            'cascade_likelihood': cascade_likelihood_ensembles,
        }
        file_name = f'{self.data_path}/cascade_likelihood_distributions_{self.name}_{p_p}_{low_s}_{p_h_start}_{p_h_stop}_{p_h_samples}_{samples_number}.npy'
        np.save(file_name, data)
        self.show_cascade_likelihood_distributions(file_name)

    def show_cascade_likelihood_distributions(self, file_name):
        data = np.load(file_name, allow_pickle=True).tolist()
        p_h_s = data['p_h_s']
        low_s = data['low_s']
        p_p = data['p_p']
        samples_number = data['samples_number']
        size = data['size']
        edges = data['edges']
        cascade_likelihood_distributions_ensembles = data['cascade_likelihood']

        legends = []
        for index, low in enumerate(low_s):
            plt.errorbar(p_h_s, cascade_likelihood_distributions_ensembles[:, index], marker='o')
            legends.append(r'$\theta_l = ' + f'{low}$')

        plt.xlabel(r'$P_h$')
        plt.ylabel('Likelihood of Cascade')
        plt.legend(legends)
        plt.savefig(
            f'{self.results_path}/cascade_likelihood_distributions_{self.name}_{p_p}_{low_s}_{p_h_s[0]}_{p_h_s[-1]}_{len(p_h_s)}_{samples_number}.jpg')
        plt.show()
